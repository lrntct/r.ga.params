#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
MODULE:    r.ga.params

AUTHOR(S): Laurent Courty

PURPOSE:    Calculate Green-Ampt infiltration parameters

COPYRIGHT: (C) 2016 by Laurent Courty

            This program is free software; you can redistribute it and/or
            modify it under the terms of the GNU General Public License
            as published by the Free Software Foundation; either version 2
            of the License, or (at your option) any later version.

            This program is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.
"""

#%option G_OPT_R_INPUT
#% key: sand
#% description: Percentage of sand
#% required: yes
#%end

#%option G_OPT_R_INPUT
#% key: clay
#% description: Percentage of clay
#% required: yes
#%end

#%option G_OPT_R_OUTPUT
#% key: texture
#% description: USDA texture classification
#% required: no
#%end

#%option G_OPT_R_OUTPUT
#% key: ef_porosity
#% description: Effective porosity
#% required: no
#%end

#%option G_OPT_R_OUTPUT
#% key: cap_pressure
#% description: Wetting front capillary pressure head
#% required: no
#%end

#%option G_OPT_R_OUTPUT
#% key: conductivity
#% description: Hydraulic conductivity
#% required: no
#%end

from __future__ import division
import sys
import os
import collections
import numpy as np

import grass.script as grass
from grass.pygrass import raster
from grass.pygrass.messages import Messenger
import grass.pygrass.utils as gutils

import classif


TEXTURE_RULES = 'texture.cat'
TEXTURE_COLORS = 'texture.colors'


class Texture(object):
    """Calculate soil texture"""

    def __init__(self, arr_clay, arr_sand):
        self.arr_clay = arr_clay
        self.arr_sand = arr_sand
        self.arr_texture = np.empty_like(self.arr_sand)

    def classify_array(self):
        """Populate texture array using sand and clay arrays"""
        classif.classif_texture(self.arr_clay, self.arr_sand, self.arr_texture)


class GAParams(object):
    """Calculate Green-Ampt parameters"""

    def __init__(self, arr_texture):
        self.arr_texture = arr_texture
        self.arr_eff_por = np.full(shape=arr_texture.shape, fill_value=np.nan, dtype = np.float32)
        self.arr_cap_pressure = np.copy(self.arr_eff_por)
        self.arr_hyd_cond = np.copy(self.arr_eff_por)

        # G-A parameters values in mm and mm/h from Rawls et al.(1983)
        Texture_class = collections.namedtuple('texture_class',
            ['sand', 'loamy_sand', 'sandy_loam', 'loam', 'silt_loam', 'silt',
            'sandy_clay_loam', 'clay_loam', 'silty_clay_loam', 'sandy_clay',
            'silty_clay', 'clay'])
        Params = collections.namedtuple('ga_params',
            ['eff_por', 'cap_pressure', 'hyd_conduct'])
        # Rawls et al.(1983) don't give values for silt. Silt loam values are used instead
        self.ga_params = Texture_class(
            sand=           Params(eff_por=0.417, cap_pressure=49.50, hyd_conduct=117.8),
            loamy_sand=     Params(eff_por=0.401, cap_pressure=61.30, hyd_conduct=29.9),
            sandy_loam=     Params(eff_por=0.412, cap_pressure=110.1, hyd_conduct=10.9),
            loam=           Params(eff_por=0.434, cap_pressure=88.90, hyd_conduct=3.4),
            silt_loam=      Params(eff_por=0.486, cap_pressure=166.8, hyd_conduct=6.5),
            silt=           Params(eff_por=0.486, cap_pressure=166.8, hyd_conduct=6.5),
            sandy_clay_loam=Params(eff_por=0.330, cap_pressure=218.5, hyd_conduct=1.5),
            clay_loam=      Params(eff_por=0.309, cap_pressure=208.8, hyd_conduct=1.0),
            silty_clay_loam=Params(eff_por=0.432, cap_pressure=273.0, hyd_conduct=1.0),
            sandy_clay=     Params(eff_por=0.321, cap_pressure=239.0, hyd_conduct=0.6),
            silty_clay=     Params(eff_por=0.423, cap_pressure=292.2, hyd_conduct=0.5),
            clay=           Params(eff_por=0.385, cap_pressure=316.3, hyd_conduct=0.3))

        self.corresp = {1: self.ga_params.sand,
                        2: self.ga_params.loamy_sand,
                        3: self.ga_params.sandy_loam,
                        4: self.ga_params.loam,
                        5: self.ga_params.silt_loam,
                        6: self.ga_params.silt,
                        7: self.ga_params.sandy_clay_loam,
                        8: self.ga_params.clay_loam,
                        9: self.ga_params.silty_clay_loam,
                        10:self.ga_params.sandy_clay,
                        11:self.ga_params.silty_clay,
                        12:self.ga_params.clay}

    def classify_array(self):
        """populate parameters arrays using arr_texture values"""
        for k, v in self.corresp.iteritems():
            self.arr_eff_por[self.arr_texture == k] = v.eff_por
            self.arr_cap_pressure[self.arr_texture == k] = v.cap_pressure
            self.arr_hyd_cond[self.arr_texture == k] = v.hyd_conduct


def read_raster_map(rast_name):
    """Read a GRASS raster and return a numpy array"""
    with raster.RasterRow(rast_name, mode='r') as rast:
        array = np.array(rast, dtype=np.uint8)
    return array

def write_raster_map(arr, rast_name, mtype):
    """Take a numpy array and write it to GRASS DB
    """
    assert isinstance(arr, np.ndarray), "arr not a np array!"
    assert isinstance(rast_name, basestring), "not a string!"
    with raster.RasterRow(rast_name, mode='w', mtype=mtype, overwrite=grass.overwrite()) as newraster:
        newrow = raster.Buffer((arr.shape[1],), mtype=mtype)
        for row in arr:
            newrow[:] = row[:]
            newraster.put_row(newrow)

def main():
    # start messenger
    msgr = Messenger()

    # Read raster maps as numpy arrays
    arr_sand = read_raster_map(options['sand'])
    arr_clay = read_raster_map(options['clay'])

    # tables files
    file_dir = os.path.dirname(__file__)
    rules_texture = os.path.join(file_dir, TEXTURE_RULES)
    colors_texture = os.path.join(file_dir, TEXTURE_COLORS)

    # generate texture map
    texture = Texture(arr_clay, arr_sand)
    texture.classify_array()

    # generate parameters map
    params = GAParams(texture.arr_texture)
    params.classify_array()

    # write maps
    if options['texture']:
        write_raster_map(texture.arr_texture, options['texture'], 'CELL')
        # add label
        grass.run_command('r.category', quiet=True, separator='pipe',
                        rules=rules_texture, map=options['texture'])
        # set 0 to NULL
        grass.run_command('r.null', quiet=True, setnull='0', map=options['texture'])
        # set color table
        grass.run_command('r.colors', quiet=True,
                        rules=colors_texture, map=options['texture'])

    if options['ef_porosity']:
        write_raster_map(params.arr_eff_por, options['ef_porosity'], 'FCELL')
    if options['cap_pressure']:
        write_raster_map(params.arr_cap_pressure, options['cap_pressure'], 'FCELL')
    if options['conductivity']:
        write_raster_map(params.arr_hyd_cond, options['conductivity'], 'FCELL')


if __name__ == "__main__":
    options, flags = grass.parser()
    sys.exit(main())
