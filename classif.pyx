# coding=utf8
"""
Copyright (C) 2015-2016 Laurent Courty

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
"""
from __future__ import division
cimport numpy as np
cimport cython
from cython.parallel cimport prange
import numpy as np
#~ from libc.math cimport pow as c_pow
#~ from libc.math cimport sqrt as c_sqrt
#~ from libc.math cimport fabs as c_abs
from libc.math cimport isnan

@cython.wraparound(False)  # Disable negative index check
@cython.cdivision(True)  # Don't check division by zero
@cython.boundscheck(False)  # turn off bounds-checking for entire function
def classif_texture(np.uint8_t [:, :] arr_clay, np.uint8_t [:, :] arr_sand,
            np.uint8_t [:, :] arr_texture):
    '''Calculate soil texture
    Category numbers:
    1 = SAND
    2 = LOAMY SAND
    3 = SANDY LOAM
    4 = LOAM
    5 = SILT LOAM
    6 = SILT
    7 = SANDY CLAY LOAM
    8 = CLAY LOAM
    9 = SILTY CLAY LOAM
    10 = SANDY CLAY
    11 = SILTY CLAY
    12 = CLAY
    '''
    cdef int rmax, cmax, r, c
    cdef int clay, sand, silt
    rmax = arr_clay.shape[0]
    cmax = arr_clay.shape[1]

    for r in range(rmax):
        for c in range(cmax):
            # define elements
            clay = arr_clay[r, c]
            sand = arr_sand[r, c]
            silt = 100 - (clay + sand)

            # define texture type boolean operation
            t_null = (clay == 0 and sand == 0)
            t_sand = (sand > 85 and ((silt + 1.5*clay) < 15))
            t_loamy_sand = ((70 < sand < 91)
                            and (silt + 1.5 * clay >= 15)
                            and (silt + 2*clay < 30))
            t_sandy_loam = (((7 <= clay < 20) and sand > 52
                            and ((silt + 2*clay) >= 30))
                            or (clay < 7 and silt < 50 and sand > 43))
            t_loam = ((7 <= clay < 27) and (28 <= silt < 50) and sand <= 52)
            t_silt_loam = ((silt >= 50 and (12 <= clay < 27))
                        or ((50 <= silt < 80) and clay < 12))
            t_silt = (silt >= 80 and clay < 12)
            t_sandy_clay_loam = ((20 <= clay < 35) and silt < 28 and sand > 45) 
            t_clay_loam = ((27 <= clay < 40) and (20 < sand <= 45))
            t_silty_clay_loam = ((27 <= clay < 40) and sand <= 20)
            t_sandy_clay = (clay >= 35 and sand >= 45)
            t_silty_clay = (clay >= 40 and silt >= 40)
            t_clay = (clay >= 40 and sand <= 45 and silt < 40)

            # classify
            if t_null:
                arr_texture[r, c] = 0
            elif t_sand:
                arr_texture[r, c] = 1
            elif t_loamy_sand:
                arr_texture[r, c] = 2
            elif t_sandy_loam:
                arr_texture[r, c] = 3
            elif t_loam:
                arr_texture[r, c] = 4
            elif t_silt_loam:
                arr_texture[r, c] = 5
            elif t_silt:
                arr_texture[r, c] = 6
            elif t_sandy_clay_loam:
                arr_texture[r, c] = 7
            elif t_clay_loam:
                arr_texture[r, c] = 8
            elif t_silty_clay_loam:
                arr_texture[r, c] = 9
            elif t_sandy_clay:
                arr_texture[r, c] = 10
            elif t_silty_clay:
                arr_texture[r, c] = 11
            elif t_clay:
                arr_texture[r, c] = 12
            else:
                arr_texture[r, c] = 99
