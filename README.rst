
===========
r.ga.params
===========

*r.ga.params* is a GRASS GIS module that outputs:

* USDA soil texture map
* Green-Ampt parameters raster maps

Description
===========

The process of classification is parallelized using Cython.
The Green-Ampt parameter affectation is done using the work of Rawls et al.(1983).
The source article don't give values for silt. Silt loam values are used instead.

The necessary source data are:

* A raster map of sand percentage ranging from 0 to 100
* A raster map of clay percentage ranging from 0 to 100

The colour scheme used for the texture map is form colorbrewer2.org

References
==========

Rawls, W.J., Brakensiek, D.L. & Miller, N., 1983.
Green-Ampt infiltration parameters from soils data.
Journal of Hydraulic Engineering, 109(1), pp.62–70.
